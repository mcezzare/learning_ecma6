// Validate all itens
var student = [
    { name: 'Joao', age: 15 },
    { name: 'Jose', age: 18 },
    { name: 'Jose', age: 22 },
    { name: 'Maria', age: 20 }
];

var allGreaterStudents = true;
for (var i = 0; i < student.length; i++) {
    if (student[i].age < 18) {
        allGreaterStudents = false;
        break;
    }
}

console.log(allGreaterStudents);
// Adds a logical AND in each item
// item1 && item2 && item3 && item4
var allGreaterStudents2 = student.every(function (aluno) {
    return aluno.age >= 18;
});
console.log(allGreaterStudents2);



