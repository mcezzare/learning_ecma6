var witches = ['Harry Potter', 'Hermione Granger', 'Rony Weasley'];

var iteratorWitches = witches[Symbol.iterator]();
console.log(iteratorWitches.next()); // {value: Harry Potter, done: false}
console.log(iteratorWitches.next()); // {value: Hermione Granger, done: false}
console.log(iteratorWitches.next()); // {value: Rony Weasley, done: false}
console.log(iteratorWitches.next()); // {value: undefined, done: true}
// Don't cause errors =)
console.log(iteratorWitches.next()); // {value: undefined, done: true}

/**
 * It always has this format:
 * { value: 'X', done: false }
 * even
 * { value: undefined, done: true }
 */

/**
 * Can be better
 */
var hatSelector = {
    houses: ['Grifnolia', 'Corvinal', 'Sonserina'],
    makeHouseSelection: function (witch) {
        let sorteio = Math.floor((Math.random() * 3));
        return this.houses[sorteio];
    }
}

var iterator = witches[Symbol.iterator]();
var done = false;
var nextItem = iterator.next();
do {
    var witch = nextItem.value;
    let house = hatSelector.makeHouseSelection(witch);
    console.log("Witch:" + witch + "|" + house);
    nextItem = iterator.next();
} while (!nextItem.done);