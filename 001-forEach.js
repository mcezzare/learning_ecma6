// Old School
var names = ['maria', 'josé', 'joão'];
for (var i = 0; i < names.length; i++) {
	console.log(names[i]);
}
console.log('\n');
// New School
names.forEach(function(nome) {
	console.log(nome);
});

console.log('\n');
function printName(nome) {
	console.log(nome);
}

names.forEach(printName);

console.log('\n');
var canais = ['Globo', 'Sbt', 'Record'];
canais.forEach(function(canal) {
	canais.push('RedeTV'); // this item will be ignored
	console.log(canal);
});

console.log(canais)