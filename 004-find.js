var students = [
    { name: 'Joao', age: 15 },
    { name: 'Jose', age: 18 },
    { name: 'Jose', age: 22 },
    { name: 'Maria', age: 20 }
];

// Returns only 1º when it has more occurrences
var student = students.find(function (student) {
    return student.name === 'Jose' && student.age == 22;
});

console.log(student);