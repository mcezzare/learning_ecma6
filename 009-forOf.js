/**
 * Only if, it will be iterable
 * Ex: Array , Set or a Map .
 */

var numbers = [1, 2, 3, 4, 5];

for (var number of numbers){
    console.log(number);
}

var faceBookProfile = {
    name : 'Strange Name for stange people',
    age : '38',
    email:  'strangeppl@strangeworld.io'
};

// Will cause an error -- TypeError: faceBookProfile is not iterable
try {
    for (var property of faceBookProfile) {
        console.log(property);
    }
    
} catch (error) {
    console.log(error);
}

// Old School way , just remembering
for (var property in faceBookProfile) {
    console.log(property);
}
console.log();
for (var property in faceBookProfile) {
    var info = faceBookProfile[property];
    console.log(property + ":" + info);
}

